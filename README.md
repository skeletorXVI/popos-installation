# Install Pop!Os 20.04 LTS

This repository holds scripts to install and setup Pop!Os 20.04 LTS for python / nodejs developers.

## Create encrypted partition

```shell
sudo cryptsetup luksFormat "/dev/$PARITION"
sudo cryptsetup luksOpen "/dev/$PARITION" linux-temp
sudo pvcreate /dev/mapper/linux-temp
sudo vgcreate pop_os /dev/mapper/linux-temp
sudo lvcreate -n root -l +100%FREE pop_os
sudo mkfs -t ext4 /dev/mapper/pop_os-root
```

## Secure Boot

https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#PreLoader
