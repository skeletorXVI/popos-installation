sudo add-apt-repository --no-update --yes "deb http://deb.debian.org/debian buster main"
sudo add-apt-repository --no-update --yes "deb http://deb.debian.org/debian buster-updates main"
sudo add-apt-repository --no-update --yes "deb http://deb.debian.org/debian-security buster/updates main"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys DCC9EFBF77E11517
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys AA8E81B4331F7F50
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 112695A0E562B32A

echo "
# Note: 2 blank lines are required between entries
 Package: *
 Pin: release a=eoan
 Pin-Priority: 500


 Package: *
 Pin: origin \"deb.debian.org\"
 Pin-Priority: 300


 # Pattern includes 'chromium', 'chromium-browser' and similarly
 # named dependencies:
 Package: chromium*
 Pin: origin \"deb.debian.org\"
 Pin-Priority: 700
" > /tmp/chromium.pref

sudo apt update
sudo apt install chromium
