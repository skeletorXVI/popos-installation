#!/bin/bash

set -erep

# --- Prepare repositories ---

# For all python versions
sudo add-apt-repository --no-update --yes ppa:deadsnakes/ppa

# For keepassxc
sudo add-apt-repository --no-update --yes ppa:phoerious/keepassxc

# For enpass
sudo add-apt-repository --no-update --yes "deb [arch=amd64] https://apt.enpass.io/ stable main"
wget --quiet --output-document "-" https://apt.enpass.io/keys/enpass-linux.key | sudo apt-key add -

# For docker
wget --quiet --output-document "-" https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository --no-update --yes "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

# For any Microsoft repositories

wget --quiet --output-document "-" https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -

# For vscode

sudo add-apt-repository --no-update --yes "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

# Microsoft Teams

sudo add-apt-repository --no-update --yes "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main"

# Nodejs 16
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -

# MongoDB

wget --quiet --output-document "-" https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -

sudo add-apt-repository --no-update --yes "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" 

# --- Update all repositories ---

sudo apt-get update --yes


# --- Install ---

sudo apt-get install --yes \
    nodejs \
    python3.8 \
    python3.9 \
    keepassxc \
    enpass \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    code \
    snapd \
    teams \
    network-manager-openconnect \
    network-manager-openconnect-gnome \
    mongodb-database-tools \
    mongocli

sudo snap install \
   postman \
   altair



# MongoDB Compass
sudo wget --quiet --output-document "/tmp/mongodb-compass_amd64.deb" https://downloads.mongodb.com/compass/mongodb-compass_1.26.1_amd64.deb
sudo apt install --yes --fix-broken /tmp/mongodb-compass_amd64.deb

# Zoom

wget --quiet --output-document "/tmp/zoom_amd64.deb" https://zoom.us/client/latest/zoom_amd64.deb
sudo apt install --yes --fix-broken /tmp/zoom_amd64.deb

# Enable corepack for node (required for yarn to avoid unnecessary install)

sudo corepack enable

# --- Setup docker ---

DOCKER_PLUGINS="/usr/local/lib/docker/cli-plugins"

sudo mkdir -p "$DOCKER_PLUGINS"
sudo wget --quiet --output-document "$DOCKER_PLUGINS/docker-compose" https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64
sudo chmod +x "$DOCKER_PLUGINS/docker-compose"

sudo systemctl enable --now docker

sudo usermod -aG docker $(id -nu "$UID")

# Jetbrains Toolbox

sudo wget --quiet --output-document "/tmp/jetbrains-toolbox.tar.gz" https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.22.10774.tar.gz
sudo tar -xzf /tmp/jetbrains-toolbox.tar.gz --directory /tmp/ --strip 1
/tmp/jetbrains-toolbox

# --- Log user out ---

gnome-session-quit --force
